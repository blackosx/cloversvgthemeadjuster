# CloverSvgThemeAdjuster

A perl script designed to adjust a theme.svg for the Clover Boot Manager.

# Overview

This script is designed to help users create simple variations of a theme for the Clover Boot Manager. This should save a theme designer creating multiple versions of the same theme, just to have it darker for example.

The script accepts adjustments for hue, saturation and brightness and then a number of iterations to loop through and progressively increased the values to produce further variations.

- Min and Max settings for hue are 0-360
- Min and Max settings for saturation are 0-100
- Min and Max settings for lightness are 0-100

## Requirements

A Theme.svg file in the same directory as the script.



## Usage

Clone the repository

```
git clone https://bitbucket.org/blackosx/cloversvgthemeadjuster.git
```

Add a Theme.svg file to the same directory and then run the script using the following:

```
perl AdjustThemeColour.pl n n n x
```

- Where n are +/- integers for hue, saturation and lightness
- And x is the number of iterations.

## Examples


```
perl AdjustThemeColour.pl 0 0 -5 5
```

Will instruct the script to adjust the lightness of the colours in Theme.svg by -5 and perform this five times, increasing the adjustment each time. So first adjustment will be -5, then -10, -15, -20 and ending with -25. If the lightness reaches the max of 100, then it will no longer be adjusted.


```
perl AdjustThemeColour.pl 20 0 10
```

Will instruct the script to adjust the hue of the colours in Theme.svg by +20 and perform this ten times, increasing the adjustment each time. So first adjustment will be 20, then 40, 60, 80 ... and so forth. If the hue reaches the max of 360, then it will no longer be adjusted.


## Links

* [Clover Scalable Themes Instructions](https://www.insanelymac.com/forum/topic/282787-clover-v2-instructions/?do=findComment&comment=2645125)
* [Clover Vector Themes](https://www.insanelymac.com/forum/topic/334659-vector-themes/)


## License

* [GPL v3](https://opensource.org/licenses/GPL-3.0)

## Acknowledgments

* [Clover Boot Manager](https://sourceforge.net/projects/cloverefiboot/)
