#!/usr/bin/env perl

# A script to adjust colours of Clover SVG themes.
# Copyright (C) 2019 Blackosx
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# v0.0.1 - 26th-28th January 2019 - Initial script.
# v0.0.2 - 28th January 2019 - Add multiple iteration option
# v0.0.3 - 29th January 2019 - Add modulo divisor if less than 0
# v0.0.4 - 30th January 2019 - Add InvertRgb and InvertChannel functions
#                            - Notify any adjusted colours that match source colours
#                            - Skip patching theme file with un-adjusted colours
#                            - Revise stdout messages
# v0.0.5 - 30th January 2019 - Add logging to file
#                            - Print number of unique colours to stdout.

use strict;
use warnings;

use List::Util qw[min max];
use POSIX "fmod";

my $VER="0.0.5";

# --------------------------------------------
sub InvertChannel {

  my ($source) = @_;

  my $flipped = $source ^ 0xff;

  return ($flipped);
}

# --------------------------------------------
sub InvertRgb {

  my ($sourceR, $sourceG, $sourceB) = @_;

  my $r = InvertChannel($sourceR);
  my $g = InvertChannel($sourceG);
  my $b = InvertChannel($sourceB);

  return ($r,$g,$b);
}

# --------------------------------------------
sub Rgb2hsl {

  my ($sourceR, $sourceG, $sourceB) = @_;
  my $hue = 0;
  my $saturation = 0;
  my $lightness = 0;
  my $tmpVal = 0;

  #print "rgb[$sourceR,$sourceG,$sourceB]\n";

  my $fineRangeR = $sourceR / 255;
  my $fineRangeG = $sourceG / 255;
  my $fineRangeB = $sourceB / 255;

  my $cMax = max($fineRangeR, $fineRangeG, $fineRangeB);
  my $cMin = min($fineRangeR, $fineRangeG, $fineRangeB);

  if ( $sourceB == $sourceG ) {
    $cMin = $fineRangeB;
  }

  my $delta = $cMax - $cMin;

  if ( $delta > 0 ) {

    if ( $cMax == $fineRangeR ) {

      $tmpVal = ( $fineRangeG - $fineRangeB ) / $delta;

      if ( $tmpVal < 0 ) {
        $tmpVal += 6;
      }

      my $tmpVal = fmod($tmpVal,6);

    } elsif ( $cMax == $fineRangeG ) {

      $tmpVal = ( $fineRangeB - $fineRangeR ) / $delta;
      $tmpVal = $tmpVal + 2;

    } else {

      $tmpVal = ( $fineRangeR - $fineRangeG ) / $delta;
      $tmpVal = $tmpVal + 4;

    }

  }

  $hue = $tmpVal * 60;

  $lightness = ( $cMax + $cMin ) / 2;

  if ( $delta > 0 ) {

    $saturation = ( $lightness * 2 ) - 1;
    $saturation = abs( $saturation );
    $saturation = $delta / ( 1 - $saturation );

  }

  $saturation = $saturation * 100;
  $lightness = $lightness * 100;

  $hue = sprintf("%.0f", $hue);
  $saturation = sprintf("%.1f", $saturation);
  $lightness = sprintf("%.1f", $lightness);

  return ($hue,$saturation,$lightness);
}

# --------------------------------------------
sub Hsl2rgb {

  my ($sourceH, $sourceS, $sourceL) = @_;
  my $c = 0;
  my $h = 0;
  my $m = 0;
  my $x = 0;
  my $r = 0;
  my $g = 0;
  my $b = 0;
  my $tmpVal = 0;

  $sourceS /= 100;
  $sourceL /= 100;

  $c = $sourceL * 2;
  $c = $c - 1;
  $c = abs( $c );
  $c = 1 - $c;
  $c = $c * $sourceS;

  $h = $sourceH / 60;

  $tmpVal = $sourceH / 60;
  $tmpVal = fmod($tmpVal,2);
  $tmpVal = $tmpVal - 1;
  $tmpVal = abs( $tmpVal );
  $tmpVal = 1 - $tmpVal;
  $x = $c * $tmpVal;

  $m = $sourceL - ( $c / 2 );

  if ( $h >= 0 && $h <= 1 ) {
    $r=$c;
    $g=$x;
    $b=0;
  }

  if ( $h >= 1 && $h <= 2 ) {
    $r=$x;
    $g=$c;
    $b=0;
  }

  if ( $h >= 2 && $h <= 3 ) {
    $r=0;
    $g=$c;
    $b=$x;
  }

  if ( $h >= 3 && $h <= 4 ) {
    $r=0;
    $g=$x;
    $b=$c;
  }

  if ( $h >= 4 && $h <= 5 ) {
    $r=$x;
    $g=0;
    $b=$c;
  }
  if ( $h >= 5 && $h <= 6 ) {
    $r=$c;
    $g=0;
    $b=$x;
  }

  $r = ( $r + $m) * 255;
  $g = ( $g + $m) * 255;
  $b = ( $b + $m) * 255;

  $r = sprintf("%.0f", $r);
  $g = sprintf("%.0f", $g);
  $b = sprintf("%.0f", $b);

  return ($r,$g,$b);
}

# --------------------------------------------
sub IsNumber {

  my ($passedString) = @_;

  if ( $passedString =~ /^[0-9]/ ) {

    return $passedString;

  } else {

    print "** $passedString is not an integer. Using zero.\n";
    return "0";

  }
}

# --------------------------------------------
sub ParseArg {

  my ($passedString) = @_;
  my $firstChar = substr( $passedString, 0, 1 );
  my $value = 0;

	if ( $firstChar =~ /^[0-9]/ ) {

		$firstChar = "+";
		$value = IsNumber($passedString);

	} elsif ( $firstChar eq "+" ) {

		$firstChar = "+";
		$value = IsNumber(substr( $passedString, 1 ));

	} elsif ( $firstChar eq "-" ) {

		$firstChar = "-";
		$value = IsNumber(substr( $passedString, 1 ));

	} else {

		$firstChar = "+";
		$value = 0;
		print "** $passedString is not an integer. Using zero.\n";

	}

  return ($firstChar,$value);
}

# --------------------------------------------
sub PrintSeparator {

  print "\n";
  print "=========================\n";
  print "\n";

}

# --------------------------------------------
sub Log {

  my ($option,$fileHandle,$message) = @_;

  if ( $option == 1 ) {

    print $message."\n";

  } elsif ( $option == 2 ) {

    print $fileHandle $message."\n";

  } elsif ( $option == 3 ) {

    print $message."\n";
    print $fileHandle $message."\n";

  }

}


# =========================
# MAIN
# =========================

PrintSeparator();

print "Clover SVG Theme Colour Adjuster v$VER\n";
print "Blackosx 2019\n";
my $datestring = localtime();
print "$datestring\n";

PrintSeparator();

# Check for the correct number of arguments

my $num_args = $#ARGV + 1;

if ($num_args != 4) {

  print "Usage: AdjustThemeColour.pl n n n x\n";
  print "- Where n are +/- integers for hue, saturation and lightness\n";
  print "- And x is the number of iterations.\n";
  print "- Min/Max values for hue: -360 to +360\n";
  print "- Min/Max values for saturation: 0 to 100\n";
  print "- Min/Max values for lightness: 0 to 100\n";
  print "- Example: AdjustThemeColour.pl 0 0 -5 5\n";
  exit;

}

# Parse arguments 

my ($hueDirection, $hueAdjustment) = ParseArg($ARGV[0]);
my ($satDirection, $satAdjustment) = ParseArg($ARGV[1]);
my ($ligDirection, $ligAdjustment) = ParseArg($ARGV[2]);

my $invertAll = 0;
my $invertR = 0;
my $invertG = 0;
my $invertB = 0;

if ( $hueAdjustment == 0 && $satAdjustment == 0 && $ligAdjustment == 0 && $invertAll == 0 && $invertR == 0 && $invertG == 0 && $invertB == 0) {

  print "All adjustment values are zero. Nothing to do. Exiting.\n";
  exit;

} else {

  print "Accepted adjustments: [$hueDirection$hueAdjustment] | saturation [$satDirection$satAdjustment] | lightness [$ligDirection$ligAdjustment]\n";

}

my $iteration = IsNumber($ARGV[3]);

if ( $iteration == 0 ) {

  print "Iteration value is zero. Nothing to do. Exiting.\n";
  exit;

} else {

  print "Accepted iteration: [$iteration]\n";

}

# Find theme.svg to process

my $file = 'Theme.svg';

if ( ! -f $file ) {

  print "Cannot find $file. Exiting\n";
  exit;

} else {

  print "Found $file\n" 

}

# Read hex values from theme file in to an array

my @thisLineHexValues;
my @totalHexValues;

open(FILE, $file ) or die "Couldn't open file theme file, $!";

while(<FILE>) {

  if ($_ =~ /#[0-9A-Fa-f]{6}/){

    @thisLineHexValues = ( $_ =~ /#[0-9A-Fa-f]{6}/g );
    push(@totalHexValues, @thisLineHexValues);

  }
}

close(FILE);

# Remove duplicates in array

my %hash   = map { $_, 1 } @totalHexValues;
my @uniqueHexValues = keys %hash;

my $UniqueCount = @uniqueHexValues;
print $UniqueCount." unique colours identifed";

#print "@uniqueHexValues\n";

# Set some vars before processing theme

my $hAdj = $hueAdjustment;
my $sAdj += $satAdjustment;
my $lAdj += $ligAdjustment;

my @fileNameSplit = split /\./,$file;

# Loop through each hex value, making adjustments

for(my $i = 1; $i <= $iteration; $i++){

  PrintSeparator();

	# Prepare log file

	my $adjustedLogFile = "$fileNameSplit[0]_adjusted [$i] ".$hueDirection.$hAdj.",".$satDirection.$sAdj.",".$ligDirection.$lAdj.".txt";
	open(my $logFile, '>', $adjustedLogFile);

  Log(3,$logFile,"Process theme [$i] h[$hueDirection$hAdj] s[$satDirection$sAdj] l[$ligDirection$lAdj]");

  my @splitHex;
  my @searchArray;
  my @replaceArray;

  foreach (@uniqueHexValues)
  {

    Log(3,$logFile,"-------------");
    Log(3,$logFile,"Source: $_ ");

    # Remove leading hash character

    my $tmp = substr $_, 1;

    # split hex value in to 3 bytes

    @splitHex = ( $tmp =~ m/../g );

    my $splitRed = hex($splitHex[0]);
    my $splitGreen = hex($splitHex[1]);
    my $splitBlue = hex($splitHex[2]);

    # Invert all RGB colours

    if ( $invertAll == 1 ) {

      Log(2,$logFile, "Inverting RGB channels.");

      ($splitRed,$splitGreen,$splitBlue) = InvertRgb($splitRed,$splitGreen,$splitBlue);

    }

    # Invert individual channels

    if ( $invertR == 1 ) {

      Log(2,$logFile, "Inverting Red channel.");

      ($splitRed) = InvertChannel($splitRed);

    }

    if ( $invertG == 1 ) {

      Log(2,$logFile, "Inverting Green channel.");

      ($splitGreen) = InvertChannel($splitGreen);

    }

    if ( $invertB == 1 ) {

      Log(2,$logFile, "Inverting Blue channel.");

      ($splitBlue) = InvertChannel($splitBlue);

    }

    # Convert to HSL

    my ($h, $s, $l) = Rgb2hsl($splitRed,$splitGreen,$splitBlue);

    Log(2,$logFile, "Before: $_ | RGB [$splitRed,$splitGreen,$splitBlue] | HSL [$h,$s,$l]");

    # Apply hue adjustment

    if ( $hAdj != 0 ) {

      if ( $hueDirection eq "-" ) {

        if ( $h - $hAdj >= 0 ) {

          Log(2,$logFile, "Adjust hue from $h by $hAdj");
          $h -= $hAdj;

        } else {

          Log(2,$logFile, "** adjusted hue < 0. Using 0");
          $h = 0;

        }

      } elsif ( $hueDirection eq "+" ) {

        if ( $h + $hAdj <= 360 ) {

          Log(2,$logFile, "Adjust hue from $h by $hAdj");
          $h += $hAdj;

        } else {

          Log(2,$logFile, "** adjusted hue > 360. using 360");
          $h = 360;

        }

      }

    }

    # Apply saturation adjustment

    if ( $sAdj != 0 ) {
  
      if ( $satDirection eq "-" ) {

        if ( $s - $sAdj >= 0 ) {

          Log(2,$logFile, "Adjust saturation from $h by $sAdj");
          $s -= $sAdj;

        } else {

          Log(2,$logFile, "** adjusted saturation < 0. Using 0");
          $s = 0;

        }

      } elsif ( $satDirection eq "+" ) {

        if ( $s + $sAdj <= 100 ) {

          Log(2,$logFile, "Adjust saturation from $h by $sAdj");
          $s += $sAdj;

        } else {

          Log(2,$logFile, "** adjusted saturation > 100. Using 100");
          $s = 100;

        }

      }

    }

    # Apply lightness adjustment

    if ( $lAdj != 0 ) {
  
      if ( $ligDirection eq "-" ) {

        if ( $l - $lAdj >= 0 ) {

          Log(2,$logFile, "Adjust lightness from $h by $lAdj");
          $l -= $lAdj;

        } else {

          Log(2,$logFile, "** adjusted lightness < 0. Using 0");
          $l = 0;

        }

      } elsif ( $ligDirection eq "+" ) {

        if ( $l + $lAdj <= 100 ) {

          Log(2,$logFile, "Adjust lightness from $h by $lAdj");
          $l += $lAdj;

        } else {

          Log(2,$logFile, "** adjusted lightness > 100. Using 100");
          $l = 100;

        }

      }

    }

    # Convert back to RGB

    my ($r, $g, $b) = Hsl2rgb($h,$s,$l);

    # Convert back to Hex

    my $hex1 = sprintf("%02X", $r);
    my $hex2 = sprintf("%02X", $g);
    my $hex3 = sprintf("%02X", $b);

    Log(2,$logFile, "After: #$hex1$hex2$hex3 | RGB [$r,$g,$b] | HSL [$h,$s,$l]");

    # Append to search and replace array, only if values differ

    if ( "$_" ne "#$hex1$hex2$hex3" ) {

      # Notify if new value exists as a source value

      if ( grep( /^#$hex1$hex2$hex3$/, @uniqueHexValues ) ) {

        Log(3,$logFile, "! Target colour #$hex1$hex2$hex3 exists in source colour list");

      }

      Log(3,$logFile, "Adjusted: #$hex1$hex2$hex3");

      push(@searchArray, "$_");
      push(@replaceArray, "#$hex1$hex2$hex3");

    } else {

      Log(3,$logFile, "No adjustment done.");

    }

  }

  # Patch and write adjusted theme to file

  my $adjustedFile = "$fileNameSplit[0]_adjusted [$i] ".$hueDirection.$hAdj.",".$satDirection.$sAdj.",".$ligDirection.$lAdj.".$fileNameSplit[1]";

  Log(3,$logFile, "\nWriting new file: $adjustedFile");

  open IN, "<",$file or die "can't open $file: $!";
  open OUT, ">",$adjustedFile or die "can't open $adjustedFile: $!";

  my $lineFromFile;
  my $arrayIdPosition;

  while(<IN>) {

    $arrayIdPosition = 0;

    $lineFromFile = $_;

    foreach (@searchArray)
    {
      #Log(3,$logFile, "loop [$arrayIdPosition]: s/$searchArray[$arrayIdPosition]/$replaceArray[$arrayIdPosition]/g)";

      $lineFromFile =~ s/$searchArray[$arrayIdPosition]/$replaceArray[$arrayIdPosition]/g;

      $arrayIdPosition ++;
    }

    print OUT "$lineFromFile";

  }

  close OUT;
  close IN;

  close $logFile;

  $hAdj = $hueAdjustment * ($i + 1);
  $sAdj = $satAdjustment * ($i + 1);
  $lAdj = $ligAdjustment * ($i + 1);

}
